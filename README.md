# Elasticsearch Test Scenarios

Outlines different code syntax that should be supported with Elasticsearch

From issue: https://gitlab.com/gitlab-org/gitlab/issues/10693

## Control file

The file named `control.md` contains terms that are currently supported in ES search. This file should always be returned when searching for a test term below.

## Types of searches that aren't currently supported with ES

Currently, terms with special characters, as well as terms containing camelCase, dots, underscores and dashes, aren't all supported in search. Here is a breakdown of the test files.

### Arrows

- Search term: `examplearrow`
- Test file with failing search: `arrow.md`
- Control file with successful search: `control.md`

### Commas

- Search term: `examplecomma`
- Test file with failing search: `comma.md`
- Control file with successful search: `control.md`

### Colons

- Search term: `examplecolon`
- Test file with failing search: `colon.md`
- Control file with successful search: `control.md`

### Double Colons

- Search term: `exampledoublecolon`
- Test file with failing search: `double_colon.md`
- Control file with successful search: `control.md`

### Equal Signs

- Search term: `exampleequalsign`
- Test file with failing search: `equal_sign.md`
- Control file with successful search: `control.md`

### Parentheses

- Search term: `exampleparenthesis`
- Test file with failing search: `parenthesis.md`
- Control file with successful search: `control.md`

### Tags

- Search term: `exampletag`
- Test file with failing search: `tag.md`
- Control file with successful search: `control.md`
